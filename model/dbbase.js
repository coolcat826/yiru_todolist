/*
Change Description                             Change Date             Change By
-------------------------------                ------------            --------------
Init Version                                   2022/04/06              Yiru Shi
*/
var mysql = require('mysql');
var conf = require('../dbconf');
var basefn = require('../support/basefn');

var dbconn = mysql.createConnection(conf.db);
var sql = '';

let dataInlength = "";

module.exports = {

    get_data: function (table_name, dataIn, callback) {//建立模組function 取得一筆資料

        WHERE = "";

        basefn.getJsonLength(dataIn, function (length) {
            dataInlength = length;
        });

        i = 0;
        for (var key in dataIn) {
            i++;
            if (i != dataInlength) {
                WHERE += key + " = '" + dataIn[key] + "' AND ";
            } else {
                WHERE += key + " = '" + dataIn[key] + "'"; //最後一筆
            }
        }

        sql = "SELECT * FROM " + table_name + " WHERE " + WHERE;

        console.log(sql);

        dbconn.query(sql, function (err, result) {
            callback(result);
            //console.log(result);
            //return result;
        });

    },

    get_datas: function (table_name, dataIn, callback) {//建立模組function 取得多筆資料
        console.log(dataIn);
        WHERE = "";
        var jsonlength = 0 ;

        for (key in dataIn){
            jsonlength++;
        }

        i = 0;
        for (var key in dataIn) {
            i++;
            if (i !== jsonlength) { // 1!=1
                WHERE += key + " = '" + dataIn[key] + "' AND ";
            } else {
                WHERE += key + " = '" + dataIn[key] + "'"; //最後一筆
            }
        }

        sql = "SELECT * FROM " + table_name + " WHERE " + WHERE + " ORDER BY " + "priority" + " ASC";;

        console.log(sql);

        dbconn.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    get_desc_limt1: function (dataIn, callback) {//抓最後一筆

        sql = "SELECT " + dataIn.field + " FROM " + dataIn.table + " WHERE " + dataIn.field + " LIKE '" + dataIn.like_field + "%' ORDER BY " + dataIn.orderfield + " Desc limit 1";
        console.log(sql);
        dbconn.query(sql, function (err, result) {
            console.log("get_desc_limt1");
            callback(result);
        });

    },

    //新增
    InsertData:function(table_spec,dataIn,callback){
        var jsonlength = 0 ;

        for (key in dataIn){
            jsonlength++;
        }

        insertSQL = "INSERT INTO " + table_spec['table_name'] ;
        Filed = "";
        Values = "";
    
        i=1;
        for(var key in dataIn){
            
            if(i != jsonlength){
                Filed += key + ",";
                Values += "'" + dataIn[key] + "',";
            }else{
                Filed += key;
                Values += "'" + dataIn[key] + "'";
            }
            i++;
        }      

        insertSQL += "(" + Filed + ") VALUES (" + Values + ")"; 

console.log("model/dbbase.js/InsertData::SQLEXE::insertSQL = " + insertSQL);     
        dbconn.query(insertSQL,function (err, results) {
console.log("model/dbbase.js/InsertData::SQLResults::Error = " + JSON.stringify(err) + " & Results" + JSON.stringify(results));   
console.log("model/dbbase.js/InsertData::END");      
            callback(err,results);
                });
    },
    UpDateData:function(table_spec,dataIn,wheredata,callback){
console.log("model/dbbase.js/UpDateData::START");
console.log("model/dbbase.js/UpDateData::CALLFN::basefn.getJsonLength");   
        var dataInlength = 0 ;

        for (key in dataIn){
            dataInlength++;
        }

        var wheredatalen=0;
        for (key in wheredata){
            wheredatalen++;
        }

        UpdateSQL = "UPDATE " + table_spec['table_name'] + " SET " ;
        DateIn = ""
        WhereData = "";
        
        i=1;
        for(var key in dataIn){
            if(i != dataInlength){
                DateIn += key+" = '" + dataIn[key] + "' , ";
            }else{
                DateIn += key+" = '" + dataIn[key] + "'";
            }
            i++;
        }
        
        i=1;
        for(var key in wheredata){
            console.log(key);
            if(i != wheredatalen){
                WhereData += key+" = '" + wheredata[key] + "' AND ";
            }else{
                WhereData += key+" = '" + wheredata[key] + "'";
            }
            i++;
        }
                
        UpdateSQL += DateIn + " WHERE " + WhereData;
console.log(UpdateSQL);
                
console.log("model/dbbase.js/UpdateSQLEXE::" + UpdateSQL);
        dbconn.query(UpdateSQL,function (err, results) {
console.log("model/dbbase.js/UpdateSQL::SQLResults::ERROR = " + JSON.stringify(err) + " & results" + JSON.stringify(results));  
console.log("model/dbbase.js/UpdateSQL::END");   
                callback(err,results);
        });
    },
    //關鍵字查詢
    get_like_and : function(table_spec,dataIn,callback){
console.log("model/dbbase.js/get_like_and::START");       
            WHERE = "";
            var dataInlength = 0 ;

            for (key in dataIn){
                dataInlength++;
            }

            if(dataIn.WHERE !== null){
                i=0;
                for(var key in dataIn){
                    
                    if(i != dataInlength){
                        WHERE += key+" LIKE '%" + dataIn[key] + "%' AND ";
                    }
                    i++;
                }
            }
            WHERE += "del = '0'";
    
        
            sql = "SELECT * FROM " + table_spec['table_name'] + " WHERE " + WHERE ;
            
console.log("model/dbbase.js/get_like_and::SQLEXE::SQL=" + sql);  
            
        dbconn.query(sql,function (err, results) {
console.log("model/dbbase.js/get_like_and::SQLResults::ERROR = " + JSON.stringify(err) + " & results" + JSON.stringify(results));  
console.log("model/dbbase.js/get_like_and::END");           
                callback(err,results);
            });
    },
}
