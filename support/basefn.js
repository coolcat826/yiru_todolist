/*
Change Description                             Change Date             Change By
-------------------------------                ------------            --------------
Init Version                                   2022/04/06              Yiru Shi
*/

var sha3 = require('sha3');
var jwt = require('jsonwebtoken');
dbbase = require('../model/dbbase');//model引入

module.exports = {
    
    getDate : function(opt,timenow,callback){
        day = getDate(opt,timenow);
        console.log(day);
        callback(day);
    },
    padLeft : function(str,length,callback){
        lstr = padLeft(str,length);
        console.log(lstr);
        callback(lstr);
    },
    padRight : function(str,length,callback){
        lstr = padRight(str,length);
        console.log(lstr);
        callback(lstr);
    },

    getJsonLength : function(jsondata,callback){
        var jsonlength = 0 ;

        for (key in jsondata){
            jsonlength++;
        }

        callback(jsonlength);
        
    },
    
    encrypt_sha3 : function(sha3str,callback){
        let hash_224 = new sha3.SHA3(224).update(sha3str).digest('hex'); 
        console.log(hash_224);
        callback(hash_224);
    },

   
    jsonwebtoken : function(dataIn,callback){
        timenow = new Date();
        exptime = Math.floor(timenow/1000)+(60*15);
        console.log("亂數",exptime);
        token = jwt.sign({dataIn,exp:exptime},'r_api_token');
        console.log(token);
        callback(token);
    },

    serial_number : function(table_spec,prefix,callback){
console.log("support/basefn/serial_number::START");
     
        table_spec = {
            table_name:table_spec['table_name'] ,
            offset:0, 
            fetch:1, 
            orderbyfield:table_spec['orderbyfield'],
            orderby:1,
            query_time_field:table_spec['query_time_field'],
        }
    
        dataIn = {
            WHERE:null,
        }
       
        dbbase.get_today_datas(table_spec,dataIn,function(err,dataOut){
console.log("support/basefn/serial_number::CALLBACK::dbbase.get_today_datas::ERROR = " + JSON.stringify(err) + " & dataOut=" + JSON.stringify(dataOut));
            if(dataOut.rowsAffected == 0){
                console.log("無資料");
                today = getDate(4);
                new_number = prefix + today + '0001';
            }else{   
                serial_number = dataOut.recordset[0].serial_number;
                console.log("今天最後一筆資料"+serial_number);
                prefix_len = prefix.length; 

                n = parseInt(serial_number.substr(prefix_len))+1;

                new_number = prefix + n;
                console.log("重新組編號"+new_number);
                
            }
            callback(new_number);
        });
console.log("support/basefn/serial_number::END");
    },
    code_number : function(table_spec,prefix,callback){
console.log("support/basefn/code_number::START");
            table_spec = {
                table_name:table_spec['table_name'] ,
                offset:0, 
                fetch:1,  
                orderbyfield:table_spec['orderbyfield'],
                orderby:1,
                query_time_field:table_spec['query_time_field'],
            }
        
            dataIn = {
                WHERE:null,
            }
            
           
            dbbase.get_last_datas(table_spec,dataIn,function(err,dataOut){
    console.log("support/basefn/code_number::CALLBACK::dbbase.get_last_datas::ERROR = " + JSON.stringify(err) + " & dataOut=" + JSON.stringify(dataOut));
                if(dataOut.rowsAffected == 0){
                    console.log("無資料");
                   
                    new_number = prefix + '0001';
                }else{   
                   
                    serial_number = dataOut.recordset[0].serial_number;
                    console.log("最後一筆資料"+serial_number);
                    
                    prefix_len = prefix.length; 

                    n = parseInt(serial_number.substr(prefix_len))+1;
                    n0 = padLeft(n,3);
                   
                    new_number = prefix + n0;
                    console.log("重新組編號"+new_number);
                    
                }
                callback(new_number);
            });
    console.log("support/basefn/serial_number::END");
        },
            
}

function getDate(opt,timenow=new Date()){
        YYYY = timenow.getFullYear();
        MM = (timenow.getMonth()+1<10 ? '0':'')+(timenow.getMonth()+1);
        DD = (timenow.getDate()<10? '0':'')+(timenow.getDate());
        hh = (timenow.getHours()<10? '0':'')+(timenow.getHours());
        mm = (timenow.getMinutes()<10? '0':'')+(timenow.getMinutes());
        ss = (timenow.getSeconds()<10? '0':'')+(timenow.getSeconds());

        if(opt == 0){
            day = timenow;
        }else if(opt == 1){
            day = YYYY + "-" + MM + "-" + DD +" "+ hh + ":" + mm + ":" + ss; 
        }else if(opt == 2){
            day = YYYY + "-" + MM + "-" + DD; 
        }else if(opt == 3){
            day = hh + ":" + mm + ":" + ss;
        }else if(opt == 4){
            day = YYYY + MM + DD; 
        }else if(opt == 5){
            day = hh + mm + ss; 
        }else{
            day = timenow;
        }
        console.log(day);
        return(day);

}

function padLeft(str,lenght){
    if(str.length >= lenght)
    return str;
    else
    return padLeft("0" +str,lenght);
}
     
function padRight(str,lenght){
    if(str.length >= lenght)
    return str;
    else
    return padRight(str+"0",lenght);
}

